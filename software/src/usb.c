#include "ftd2xx.h"
#include "WinTypes.h"
#include "string.h"
#include "top.h"
#include "unistd.h"

/* extern DWORD IsOpen[NumMaxOfDevices]; */
/* extern Card AllCards[NumMaxOfDevices]; */
DWORD AmountInRxQueue[NumMaxOfDevices];
DWORD AmountInTxQueue[NumMaxOfDevices];
DWORD EventStatus[NumMaxOfDevices];
char SerialNumber[16];
char Description[64];


Card AllCards[NumMaxOfDevices] = {
    {.IsOpen = 0},
    {.IsOpen = 0},
    {.IsOpen = 0},
    {.IsOpen = 0},
    {.IsOpen = 0},
};

/////////////
// STATUS  //
/////////////

ULONG _UpdateRxTxStatusUSB(int CardNb){

    ftStatus = FT_GetStatus(ftHandle[CardNb], AmountInRxQueue + CardNb, AmountInTxQueue + CardNb, EventStatus + CardNb);
    checkout

	return 0;
}

ULONG GetRxTxStatusUSB(int CardNb, LPDWORD myAmountInRxQueue, LPDWORD myAmountInTxQueue, LPDWORD myEventStatus){

    ftStatus = _UpdateRxTxStatusUSB(CardNb);
    checkout

    *myAmountInRxQueue	= AmountInRxQueue[CardNb];
    *myAmountInTxQueue	= AmountInRxQueue[CardNb];
    *myEventStatus	= EventStatus[CardNb];

    return 0;
}

ULONG GetNumberDevs(LPDWORD myNumberDevs){

    ftStatus = FT_CreateDeviceInfoList(&NumberDevs);
    checkout

	*myNumberDevs = NumberDevs>>1;

    return 0;
}

ULONG GetDescriptors(char mySerialNumber[16], char myDescription[64]){

    strcpy(mySerialNumber, SerialNumber);
    strcpy(myDescription, Description);

    return 0;

}

////////////////
// OPEN/CLOSE //
////////////////

ULONG InitializeUSB(int CardNb){

    // create the device information list
    ftStatus = FT_CreateDeviceInfoList(&NumberDevs);
    checkout
	sleep(.1);

    ftStatus = FT_GetDeviceInfoDetail(CardNb, Flags + CardNb, Type + CardNb, ID + CardNb, LocId + CardNb,
				      SerialNumber, Description, ftHandle + CardNb);
    checkout
	sleep(.1);

    ftStatus = FT_Open(CardNb, ftHandle + CardNb);
    checkout
	sleep(.1);

    AllCards[CardNb].IsOpen = 1;

    return 0;
}

ULONG FinalizeUSB(int CardNb){

    FT_Close(ftHandle[CardNb]);
    checkout

	AllCards[CardNb].IsOpen = 0;

    return 0;
}

ULONG ConfigureUSB(int CardNb){

    ftStatus = FT_ResetDevice(ftHandle[CardNb]);
    checkout

	ftStatus = FT_SetTimeouts(ftHandle[CardNb], ReadTimeout, WriteTimeout);
    checkout

	// Get Bit Mode
	ftStatus = FT_GetBitMode(ftHandle[CardNb], &BitMode);
    checkout
	sleep(.1);

    // Reset Bit Mode
    ftStatus = FT_SetBitMode(ftHandle[CardNb], Mask, 0x00);
    checkout
	sleep(.1);

    // Set Bit Mode
    ftStatus = FT_SetBitMode(ftHandle[CardNb], Mask, Mode);
    checkout
	sleep(.1);

    // Get Latency Timer
    ftStatus = FT_GetLatencyTimer(ftHandle[CardNb], &LatencyTimer);
    checkout

	// Set Latency Timer
	UCHAR LatencyTimer2 = 2;
    ftStatus = FT_SetLatencyTimer(ftHandle[CardNb], LatencyTimer2);
    checkout

	// Set USB Parameters
	ftStatus = FT_SetUSBParameters(ftHandle[CardNb], InTransferSize, OutTransferSize);
    checkout

	// Flow Control
	ftStatus = FT_SetFlowControl(ftHandle[CardNb], FT_FLOW_RTS_CTS, uXon, uXoff);
    checkout

	return 0;
}

ULONG GetIsOpen(int CardNb, DWORD *myIsOpen){

    *myIsOpen = AllCards[CardNb].IsOpen;

    return 0;
}

///////////////
// SEND/READ //
///////////////

ULONG _NullCommand(int CardNb){

    // null command
    char lpBuffer[2];
    lpBuffer[0] = 0x00;
    lpBuffer[1] = 0xFF;

    ftStatus = WriteUSB(CardNb, (void *)lpBuffer, 2, &BytesWritten);
    checkout

	return 0;
}

ULONG WriteUSB(int CardNb, void * Buffer, DWORD BytesToWrite, LPDWORD BytesWritten){

    ftStatus = FT_Write(ftHandle[CardNb], Buffer, BytesToWrite, BytesWritten);
    checkout
	sleep(.5);

    if (*BytesWritten != BytesToWrite)
	return 20;

    return 0;
}


ULONG ReadUSB(int CardNb, LPVOID Buffer, DWORD BytesToRead, LPDWORD BytesRead){

    ftStatus = FT_Read(ftHandle[CardNb], Buffer, BytesToRead, BytesRead);
    checkout
    if (*BytesRead != BytesToRead)
	return 50;

    return 0;
}

ULONG PurgeRxTxUSB(int CardNb){

    int i = 0;

    for (i=0;i<2;i++){

	// do { ftStatus = FT_StopInTask(ftHandle[CardNb]); } while (ftStatus != FT_OK);

	ftStatus = _NullCommand(CardNb);
	checkout
	    sleep(.2);

	// purge
	ftStatus = FT_Purge(ftHandle[CardNb], FT_PURGE_RX | FT_PURGE_TX);
	checkout
	    //Sleep(200);

	    // do { ftStatus = FT_RestartInTask(ftHandle); } while (ftStatus != FT_OK);

	    }

    return 0;
}
